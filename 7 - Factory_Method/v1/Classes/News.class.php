<?php

class News extends Publication
{
        protected $source;

        public function __construct( $caption, $text, $preview, $source )
        {
            parent::__construct( $caption, $text, $preview);
            $this->source = $source;
        }


    public function getAllView()
    {
        return $this->getCaption() .'<hr>'. $this->getText() .'<hr>'. $this->source;
    }
}