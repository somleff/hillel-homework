<?php

class Publication
{
    protected $id;
    protected $caption;
    protected $text;
    protected $preview;


    public function __construct( $caption, $text, $preview)
    {
        $this->caption = $caption;
        $this->text = $text;
        $this->preview = $preview;
    }


    public function getId()
    {
        return $this->id;
    }


        public function setId($id)
        {
            $this->id = $id;
        }


    public function getCaption()
    {
        return $this->caption;
    }


        public function getText()
        {
            return $this->text;
        }


            public function getPreview()
            {
                return $this->preview;
            }


    public function getShortPreview()
    {
        return $this->getCaption() .'<br>'. $this->getPreview();
    }


    public function getAllView()
    {
        return $this->getCaption() .'<br>'. $this->getText();
    }

    public static function create($id, PDO $pdo)
    {
        $sql = 'SELECT * FROM publication WHERE id = :id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue( ':id', $id );
        $stmt->execute();

        $row = $stmt->fetchObject();
        switch( $row->type )
        {
            case 'news':
                $publication = new News(
                    $row->caption,
                    $row->text,
                    $row->preview,
                    $row->source);
                break;
            case 'article':
                $publication = new Articles(
                    $row->caption,
                    $row->text,
                    $row->preview,
                    $row->author);
                break;
            default:
                return NULL;
                break;
        }

        $publication->setId($id);
        return $publication;
    }
}