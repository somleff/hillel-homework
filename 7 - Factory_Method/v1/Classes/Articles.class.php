<?php

class Articles extends Publication
{
        protected $author;

        public function __construct( $caption, $text, $preview, $author )
        {
            parent::__construct( $caption, $text, $preview);
            $this->author = $author;
        }


        public function getAllView()
    {
        return $this->getCaption() .'<hr>'. $this->getText() .'<hr>'. $this->author;
    }
}