<?php

#Заполняем таблицу tour_list инфой
try
{
    $sql = 'INSERT INTO publication SET
            caption = "APPLE AIRPOWER WIRELESS CHARGING PAD: EVERYTHING YOU NEED TO KNOW",
            text = "Apple’s new AirPower is a wireless charging pad to help clean up the mess of charging cables for your iPhone, Apple Watch, and AirPods. Apple unveiled AirPower at its iPhone X Event, but the company didn’t provide much detail. We hope to fill the void here. We’ll update this article as we get more details, so stay tuned.
                AirPower is a wireless charging pad. There are several wireless charging pads on the market, but before the iPhone 8 and iPhone X, you had to use a special case with your iPhone in order to use a wireless charging pad.

                AirPower works without the need of a special case. Just plop your iPhone on to the AirPower pad, and it starts charging. No need to plug in your iPhone.
                Place the AirPower pad on your desk or wherever you like to charge your devices. Then plug it into a power outlet. To charge your device, just place it on the mat, with front facing up. That’s it.

                To get a little but more technical, AirPower uses induction, where an electromagnetic field is used to transfer power from (in this case) the AirPower to a device. When you place your device on the AirPower, it receives a signal from the pad to basically ‘handshake” with the device (checks for compatibility, charge capacity, etc). If everything checks out, charging proceeds.",
            preview = "Apple’s new AirPower is a wireless charging pad to help clean up the mess of charging cables for your iPhone, Apple Watch, and AirPods.",
            type = "news",
            source = "Macworld"';
    $pdo->exec($sql);
}
catch (PDOException $e)
{
    echo 'Error upgrade tour_list table: ' . $e->getMessage();
}

#________________________________________
try
{
       $sql = 'INSERT INTO publication SET
            caption = "HDR TV: EVERYTHING YOU NEED TO KNOW BEFORE YOU SHOP FOR A NEW 4K TELEVISION",
            text = "HDR (High Dynamic Range) is the big buzzword in TVs right now, with vendors adding support for HDR content and pasting the acronym all over their packaging and advertising. But the fact is, a TV’s ability to decode and render HDR content doesn’t necessarily mean you’ll see an improvement in image quality. The “you get what you pay for” axiom comes into play here: Poorly implemented HDR, typically caused by less-expensive TVs being outfitted with insufficient technology, can make HDR content less appealing.
                High dynamic range simply means there’s a greater difference in lumens (a unit of measure of brightness, to grossly oversimplify) between the darkest hue on the TV’s screen and the brightest. Many less-expensive TVs simply can’t conjure up that difference.

                This difference between the brightest white and the darkest black is also referred to as the TV’s contrast ratio. Vendors avoid the measurement like the plague. That’s partially because contrast ratio depends on the display settings when the measurement is taken, and partially because the contrast ratio spec can make the TV seem really bad statistically—worse than it actually is. By the same token, it can also make the TV seem much better than it actually is.
",
            preview = "You will find high dynamic range at every price point now, and that is made the technology is strengths, differences, and pitfalls abundantly clear.",
            type = "news",
            source = "TechHive"';
    $pdo->exec($sql);
}
catch (PDOException $e)
{
    echo 'Error upgrade tour_list table: ' . $e->getMessage();
}

#________________________________________И так далее ;)

