<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('db&sql/db.inc.php');
require_once('Classes/Publication.class.php');
require_once('Classes/News.class.php');
require_once('Classes/Articles.class.php');


$publication1 = Publication::create( 1, $pdo );
$publication2 = Publication::create( 2, $pdo );
$publication3 = Publication::create( 3, $pdo );
$publication4 = Publication::create( 4, $pdo );

echo $publication1->getShortPreview() .'<br>'."<a href=view.php?id=1>Read More</a>";
echo "<br>";
echo "<br>";
echo $publication2->getShortPreview() .'<br>'."<a href=view.php?id=2>Read More</a>";
echo "<hr>";
echo $publication3->getShortPreview() .'<br>'."<a href=view.php?id=3>Read More</a>";
echo "<br>";
echo "<br>";
echo $publication4->getShortPreview() .'<br>'."<a href=view.php?id=4>Read More</a>";
echo "<hr>";
