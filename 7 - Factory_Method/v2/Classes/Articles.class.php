<?php

class Articles extends Publication
{
        public $author;

        public function __construct( $id, $caption, $text, $preview, $author )
        {
            parent::__construct( $id, $caption, $text, $preview);
            $this->author = $author;
        }


        public function getAllView()
    {
        echo parent::getAllView(). '<br>'. 'Author: '.$this->author; 
    }
}