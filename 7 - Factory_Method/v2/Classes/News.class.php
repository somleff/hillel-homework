<?php

class News extends Publication
{
        public $source;

        public function __construct( $id, $caption, $text, $preview, $source )
        {
            parent::__construct( $id, $caption, $text, $preview);
            $this->source = $source;
        }


    public function getAllView()
    {
        echo parent::getAllView(). '<br>'. 'Source: '.$this->source; 
    }
}