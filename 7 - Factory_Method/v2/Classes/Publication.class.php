<?php

class Publication
{
    public $id;
    public $caption;
    public $text;
    public $preview;


    public function __construct( $id, $caption, $text, $preview)
    {
        $this->id = $id;
        $this->caption = $caption;
        $this->text = $text;
        $this->preview = $preview;
    }


    public function getId()
    {
        return $this->id;
    }


        public function setId($id)
        {
            $this->id = $id;
        }


    public function getCaption()
    {
        echo $this->caption.'<br>';
    }


        public function getText()
        {
            echo $this->text;
        }


            public function getPreview()
            {
                echo $this->preview;
            }


    public function getShortPreview()
    {
        return $this->getCaption() .'<br>'. $this->getPreview();
    }


    public function getAllView()
    {
    
        echo $this->getCaption() . $this->getText();
    }

    public static function create($id, PDO $pdo)
    {
        $sql = 'SELECT * FROM publication WHERE id = :id';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue( ':id', $id );
        $stmt->execute();

        $row = $stmt->fetchObject();

        switch( $row->type )
        {
            case 'news':
                $publication = new News(
                    $row->id,
                    $row->caption,
                    $row->text,
                    $row->preview,
                    $row->source);
                break;

            case 'article':
                $publication = new Articles(
                    $row->id,
                    $row->caption,
                    $row->text,
                    $row->preview,
                    $row->author);
                break;
                
            default:
                return NULL;
                break;
        }

        $publication->setId($id);
        return $publication;
    }
}