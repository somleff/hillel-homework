<?php

class PublicationsWriter
{
    public $publicationType = array();


    public function __construct( PDO $pdo, $type )
    {
        $sql = 'SELECT * FROM publication WHERE type = :type';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue( ':type', $type );
        $stmt->execute();

        $row = $stmt->fetchAll();

        foreach( $row as $key => $value)
        {
           switch( $value['type'] )
            {
            case 'news':
                $this->publicationType[] = new News(
                    $value['id'],
                    $value['caption'],
                    $value['text'],
                    $value['preview'],
                    $value['source']);
                break;

            case 'article':
                $this->publicationType[] = new Articles(
                    $value['id'],
                    $value['caption'],
                    $value['text'],
                    $value['preview'],
                    $value['source']);
                break;

            default:
                return NULL;
                break;
            } 
        }
    }


    public function getPublication()
    {
        foreach($this->publicationType as $key => $value)
        {
            echo $value->caption .'<br>' .$value->preview .'<br>' .'<a href="view.php?id= '.$value->id.' ">Read More</a>' .'<br>'.'<br>';
        }
    }
}