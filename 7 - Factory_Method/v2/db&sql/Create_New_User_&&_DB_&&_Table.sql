CREATE USER 'dima'@'localhost' IDENTIFIED BY '***';GRANT USAGE ON *.* TO 'dima'@'localhost' IDENTIFIED BY '***' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;

REVOKE ALL PRIVILEGES ON *.* FROM 'dima'@'localhost'; GRANT ALL PRIVILEGES ON *.* TO 'dima'@'localhost' REQUIRE NONE WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;

CREATE DATABASE news&articles;

CREATE TABLE `news&articles`.`publication` ( `id` INT NOT NULL , `caption` VARCHAR(255) NOT NULL , `text` TEXT NOT NULL , `type` VARCHAR(50) NOT NULL , `author` VARCHAR(100) NOT NULL , `source` VARCHAR(100) NOT NULL ) ENGINE = InnoDB;

//Заполняем таблицу

INSERT INTO `news&articles`.`publication` (`id`, `caption`, `text`, `prewie`, `type`, `author`, `source`) VALUES (NULL, '', '', '', '', '', '');

