@extends('template')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-8 col-offset-2">
                <h1>{{ $page->title }}</h1>
                <h5>{{ $page->intro }}</h5>
                <p>{{ $page->content }}</p>
            </div>

        </div>
    </div>
@endsection