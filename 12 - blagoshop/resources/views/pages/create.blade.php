@extends('template')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Create New Page:</h2>
                <form method="POST" action="/pages">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="title" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="alias">Alias</label>
                        <input type="text" name="alias" id="alias" class="form-control">
                    </d iv>
                    <div class="form-group">
                        <label for="intro">Intro:</label>
                        <textarea name="intro" id="intro" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="body">Content:</label>
                        <textarea name="content" id="content" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary"> Save </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection