@extends('template')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Edit Page:</h2>
                <form method="POST" action="/pages/{{$page->alias}}">
                    {{method_field('PUT')}}
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input value="{{$page->title}}" type="text" name="title" id="title" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="alias">Alias</label>
                        <input value="{{$page->alias}}" type="text" name="alias" id="alias" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="intro">Intro:</label>
                        <textarea name="intro" id="intro" class="form-control">{{$page->intro}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="content">Content:</label>
                        <textarea name="content" id="content" class="form-control">{{$page->content}}</textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary"> Save </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection