@extends('template')

@section('content')
<div class="container" align="center">
    <a href="/pages/create"><h3>Create New Page</h3></a><br>
</div>

<div class="container">
    <div class="row">

        @foreach($pages as $page)
            <div class="col-md-4">
                <h2> {{ $page->title }} </h2>
                <p>
                    {{ $page->intro }}
                </p>

                <a href="/pages/{{$page->alias}}" class="btn btn-default">Read more</a>
                <a href="/pages/{{$page->alias}}/edit" class="btn btn-warning">Edit</a>
                <a href="/pages/{{$page->alias}}/delete" class="btn btn-danger">Delete</a>
            </div>
        @endforeach

    </div>
</div>
@endsection