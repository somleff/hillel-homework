@extends('template')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-offset-2">
                <h1>Do you want to delete {{ $product->title }} ?</h1>
                <form action="/products/{{$product->alias}}" method="POST">
                    {{method_field('DELETE')}}
                    {{csrf_field()}}
                    <div class="form-controll">
                        <button class="btn btn-danger">DELETE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection