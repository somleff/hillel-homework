@extends('template')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Edit Item:</h2>
                <form method="POST" action="/products/{{$product->alias}}">
                    {{method_field('PUT')}}
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input value="{{$product->title}}" type="text" name="title" id="title" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="alias">Alias</label>
                        <input value="{{$product->alias}}" type="text" name="alias" id="alias" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="price">Price:</label>
                        <textarea name="price" id="price" class="form-control">{{$product->price}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea name="description" id="description" class="form-control">{{$product->description}}</textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary"> Save </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection