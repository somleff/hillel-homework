@extends('template')

@section('content')
<div class="container" align="center">
    <a href="/products/create"><h3>Add New Product</h3></a><br>
</div>

<div class="container">
    <div class="row">

        @foreach($products as $product)
            <div class="col-md-4">
                <h2> {{ $product->title }} </h2>
                <p>
                    Price: <strong>{{$product->price}}</strong>
                </p>

                <a href="/products/{{$product->alias}}" class="btn btn-default">Read more</a>
                <a href="/products/{{$product->alias}}/edit" class="btn btn-warning">Edit</a>
                <a href="/products/{{$product->alias}}/delete" class="btn btn-danger">Delete</a>
            </div>
        @endforeach

    </div>
</div>
@endsection