@extends('template')

@section('content')
<div class="container">
    <div class="row">

        @foreach($orders as $order)
            <div class="col-md-4">
                <h2> {{ $order->customer_name }} </h2>
                <p>
                    <b>Email: </b>{{ $order->email }}
                </p>
                <p>
                    <b>Phone: </b>{{ $order->phone }}
                </p>
                <p>
                    Customer Feedback: 
                <h5> "{{ $order->feedback }}" </h5>
                </p>
            </div>
        @endforeach

    </div>
</div>
@endsection