<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        $pages = Page::all();
        return view( 'pages/index', compact('pages'));
    }


    public function show(Page $page)
    {
        return view('pages/show', compact('page'));
    }


    public function create()
    {
        return view('pages/create');
    }


        public function store()
        {
            $this->validate(request(),
        [
            'title' => 'required|string',
            'alias' => 'required|unique:pages',
            'intro' => 'required',
            'content' => 'required'
        ]);
            Page::create(request()->all());
            return redirect('/pages');
         }


    public function delete(Page $page)
    {
        return view('pages/delete', compact('page'));
    }


            public function destroy(Page $page)
            {
                $page->delete();
                return redirect('/pages');
            }


    public function edit(Page $page)
    {
        return view('pages/edit', compact('page'));
    }


            public function update(Page $page)
            {
                $page->update(request()->all());
                $this->validate(request(),[
                    'title' => 'required|string',
                    'alias' => 'required',
                    'intro' => 'required',
                    'content' => 'required'
                ]);
                return redirect('/pages');
            }
}