<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view( 'products/index', compact('products'));
    }


    public function show(Product $product)
    {
        return view('products/show', compact('product'));
    }


     public function create()
    {
        return view('products/create');
    }


        public function store()
        {
            $this->validate(request(),
        [
            'title' => 'required|string',
            'alias' => 'required|unique:products',
            'price' => 'required',
            'description' => 'required'
        ]);
            Product::create(request()->all());
            return redirect('/products');
         }


    public function delete(Product $product)
    {
        return view('products/delete', compact('product'));
    }


            public function destroy(Product $product)
            {
                $product->delete();
                return redirect('/products');
            }


    public function edit(Product $product)
    {
        return view('products/edit', compact('product'));
    }


            public function update(Product $product)
            {
                $product->update(request()->all());
                $this->validate(request(),[
                    'title' => 'required|string',
                    'alias' => 'required',
                    'price' => 'required',
                    'description' => 'required'
                ]);
                return redirect('/products');
            }
}
