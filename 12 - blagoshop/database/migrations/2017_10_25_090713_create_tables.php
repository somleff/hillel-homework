<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('alias');
            $table->decimal('price', 5, 2);
            $table->text('description');
            $table->timestamps();
        });


        Schema::create('orders', function (Blueprint $table){
            $table->increments('id');
            $table->string('customer_name', 100);
            $table->string('email', 128);
            $table->string('phone');
            $table->text('feedback');
            $table->timestamps();
        });


        Schema::create('pages', function (Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->text('alias');
            $table->text('intro');
            $table->text('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('pages');
    }
}