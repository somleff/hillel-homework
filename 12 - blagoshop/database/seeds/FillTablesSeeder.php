<?php

use Illuminate\Database\Seeder;

class FillTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert
        ([
            [
                'title'=>'Lewis',
                'alias'=>'levis_wine',
                'price'=>'90',
                'description'=>'Remarkably elegant and refined for a wine of this size and depth, with tiers of plum, blackberry and currant flavors, as well as subtle touches of black licorice that remain pure and graceful on the long, lingering aftertaste.',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],

            [
                'title'=>'Domaine Serene',
                'alias'=>'domaine_serene_wine',
                'price'=>'55',
                'description'=>'Focused and expressive, this is layered with wet stone, tangy citrus, pear and green guava flavors that swirl through an expansive finish, gaining momentum and extra dimensions with each sip.',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],

            [
                'title'=>'Beaux Frères',
                'alias'=>'beaux_freres_wine',
                'price'=>'55',
                'description'=>'Supple, expressive and multilayered, exhibiting flavors of plum, currant, pomegranate and violet that come together harmoniously, persisting into the long and exceptionally well-balanced finish.',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        ]);


        DB::table('orders')->insert
        ([
            [
                'customer_name'=>'Baron deBua',
                'email'=>'debua@dynasty.com',
                'phone'=>'+3932003444',
                'feedback'=>'My respect to this store.',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],

            [
                'customer_name'=>'Mr President',
                'email'=>'usaforever@country.com',
                'phone'=>'+3911',
                'feedback'=>'Marilyn is delighted with your products;)',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],

            [
                'customer_name'=>'Lord of The Rings',
                'email'=>'mountain@mordor.com',
                'phone'=>'+0000000000',
                'feedback'=>'I sad eagle delivery, please!',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        ]);

        DB::table('pages')->insert
        ([
            [
                'title'=>'WHEN DOES VINTAGE MATTER MOST?',
                'alias'=>'when_does_vintage_matter_most',
                'intro'=>'A wine’s vintage is simply the calendar year that the grapes were grown and harvested. Neatly tagged on bottle labels, the vintage year represents one clue, among many, as to what’s going on inside of a particular bottle. Think of a wine’s vintage year as its birth year, and while we may associate certain events from a given year personally, producers tend to recall tricky weather patterns from a demanding year first and foremost. A wine’s vintage is a collective mirror of the weather patterns, vineyard management and state of the vine in each growing season, with climate conditions typically playing the biggest role in determining what kind of crop rolls through the cellar doors at harvest.',
                'content'=>'Location, Location, Location – Where Geography and Climate Collides

                        If vintage reflects a region’s weather patterns in a given year, then what makes a vintage good or bad? It often boils down to sunshine. Similar to “good” or “bad” vacation weather, the best vintages have plenty of dry, warm, sunny days with cooler, sweater-themed evenings. It’s these sunny, happy weather days that give grapes the best chance of reaching full maturity and optimum ripeness levels. These cheery growing seasons carrying sunny days and cool nights are the vintage years that typically garner the best ratings. If a region is bogged down by constant clouds and rain in a growing season, then the grapes are less likely to fully ripen, may be more prone to rot and disease, and tend to deliver skewed sugar and acidity levels if soggy conditions persist through harvest. These are the years where winemakers must work their magic in the cellar to keep the final wines intact. Keep in mind, certain grape varieties crave specific climates. Riesling prefers the cooler climes of Germany’s more northerly latitudes where acidity levels remain quite high. While, Cabernet Sauvignon’s roots dig deep in sunny, warm soils, and these thick-skinned grapes tend to thrive in California’s long, historically warm growing season.
                        When Vintage Matters Most

                        Crazy Climates: Vintage matters most in winegrowing regions with the least consistent weather patterns. In general, many of Europe’s more northerly winegrowing regions (France, Germany, Austria, Northern Italy, Northwest Spain) are subject to more meteorology madness than the New World’s sun-drenched surroundings. From late spring frost, where entire vineyards can be taken out before bud break begins to severe hail knocking buds off the vine before they have a chance to set fruit, to excessive rainfall near harvest, which dilutes innate sugar and acidity levels, wicked weather can quickly take its toll on the vine. Ironically, France the iconic birthplace of modern wine is home to some of the least predictable runs of weather. Bordeaux, located 30 miles southeast from the harried Atlantic coastline, is notorious for battling all sorts of weather-induced mayhem, especially as the fall harvest draws near. Likewise, Burgundy’s bud break is often the target of regional spring hailstorms that wipe out flowers before the fruit ever has a chance to set.
                        Collecting Wine: Vintage matters most when collectors are buying wine to age. The best vintages create wines that carry high levels of tannin and acidity, both are must-have preservatives when considering the age-worthiness of a wine. That’s why high-end reds from Bordeaux, Burgundy, Tuscany, Piedmont, Rioja, the Douro and New World regions like California, Washington, Australia, Argentina and Chile from the best vintage years have the greatest aging potential. In the case of white wines, Germany’s high end Rieslings, Champagne’s vintage bubbly, along with premium white wines from the likes of the Loire Valley, Alsace, Alto Adige and Burgundy all pin their aging potential on the quality of the vintage.',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],

            [
                'title'=>'ARE ORGANIC WINES REALLY ORGANIC?',
                'alias'=>'organic_wines_really_organic',
                'intro'=>'As wine consumers, we have learned to ask a lot of questions about what we are drinking. What exactly is in this bottle? Are pesticides or herbicides sprayed on the grapes used to make this wine? Is anything added into the wine in the winery? Are any organisms or the environment harmed to make this? The laws that govern sustainable wine growing and processing can actually be quite tricky. We need to understand how to ask our questions before we can understand the answers.',
                'content'=>'What is sustainable farming?

                        Wine Spectator gives a thorough definition of sustainable as it relates to the production of wine.

                        “Sustainability refers to a range of practices that are not only ecologically sound, but also economically viable and socially responsible. (Sustainable farmers may farm largely organically or biodynamically but have flexibility to choose what works best for their individual property; they may also focus on energy and water conservation, use of renewable resources and other issues.) Some third-party agencies offer sustainability certifications, and many regional industry associations are working on developing clearer standards.”

                        The sustainable label is useful; it tells the consumer which wines are made with ecological, economical, and social principles in mind. Its limitation is that it is locally defined and therefore varies regionally.

                        What is organic wine?

                        “Organic” is a system of farming and food processing, as well as a label. In the USA, organic is regulated by the National Organic Program (NOP) of the Department of Agriculture’s (USDA) Agricultural Marketing Service (AMS), in accordance with the Organic Foods Production Act of 1990 (OFPA). These entities ensure uniform and reliable standards.

                        By definition, organic farming and food processing integrates cultural, biological, and mechanical practices that foster the cycling of resources, promote ecological balance, and conserve biodiversity. Synthetic fertilizers, sewage sludge, irradiation, and genetic engineering are not allowed. Products from outside of the cycle are used minimally.',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],

            [
                'title'=>'A BALANCED BOOKSHELF: RECOMMENDED READING FOR EVERY WINE LOVER',
                'alias'=>'recommended_reading_wine_lovers',
                'intro'=>'Every wine lover, from the novice drinker to the seasoned professional, knows that the world of wine can be an intimidating one. Between regions, grape varieties, science, history, and more, there is a never-ending world of knowledge to be uncovered. Though it may seem overwhelming at times, learning about wine can be an exciting and fulfilling lifelong pursuit. Fortunately, there is a wealth of reading material out there to shed light on the universe of vines, grapes, and wines, and the people responsible for bringing them to your glass. In today’s fast-paced world of digital media, most people rely on blogs and online publications for information—but there’s nothing like a great book to enhance one’s understanding of a subject, whether it is a broad survey or a deep dive into a specific topic. We have curated a list of some of our favorite books about wine, with something for everyone, whatever your specific interests may be.',
                'content'=>'For the Beginner:

                        Wine Folly: The Essential Guide to Wine
                        Madeline Puckette
                        The colorful illustrations and infographics in this book are about as approachable and unpretentious as it gets. If you’re starting from square one and feeling overwhelmed by the prospect of learning about wine, this beautifully designed guide will instantly put you at ease and make your studies as fun as they are informative.

                        Windows on the World Complete Wine Course
                        Kevin Zraly
                        This is a solid overview of the world’s wine regions and grapes, structured like an Intro to Wine course. If you don’t have the time or money to take a wine class in person, this is the next best thing, and its various editions published over the last 25 years have been the gold standard in basic wine education texts.
                        For the Generalist:

                        The World Atlas of Wine
                        Hugh Johnson
                        Now in its seventh edition, this is the holy grail reference book for the wine regions of the world. Filled with colorful, detailed maps and high-quality photos, this is the best resource for understanding the “sense of place” behind the wine in your glass.

                        The Oxford Companion to Wine
                        Jancis Robinson & Julia Harding
                        This indispensable volume is a must-own for every serious wine lover. With alphabetized entries covering just about every topic imaginable, you likely won’t be reading this one from cover to cover. But if you’re in need of a quick overview of,  say, canopy microclimate, the Vermentino grape, or the wines of Bulgaria, this is the first place you’ll want to look.
                        For the Storyteller:

                        Judgement of Paris
                        George M. Taber
                        A must-read for any lover of Californian wine, this story chronicles the now-infamous Paris Tasting of 1976, in which wines from Stag’s Leap Wine Cellars and Chateau Montelena beat out their French counterparts in a blind tasting judged by French wine experts. The event caused quite an uproar, and was responsible for putting the Napa Valley in a prime spot on the international wine stage. This compelling account of the tasting as well as the events preceding and following it is written by the sole journalist who reported on the tasting.

                        A Hedonist in the Cellar: Adventures in Wine
                        Jay McInerney
                        This guy knows how to have fun with wine. In a serious of short essays, the beloved novelist and bon vivant who wrote Bright Lights, Big City brings you along on his imbibing adventures with a lively, colorful, and at times irreverent writing style. It’s such a delight to read, you might not even notice how informative it is.',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        ]);
    }
}