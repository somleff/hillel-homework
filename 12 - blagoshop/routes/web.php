<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostsController@index');


Route::get('/products', 'ProductsController@index');
Route::get('/products/create', 'ProductsController@create');
Route::post('/products', 'ProductsController@store');
Route::get('/products/{product}' , 'ProductsController@show');
Route::get('/products/{product}/delete', 'ProductsController@delete');
Route::delete('/products/{product}', 'ProductsController@destroy');
Route::get('/products/{product}/edit', 'ProductsController@edit');
Route::put('/products/{product}', 'ProductsController@update');


Route::get('/pages', 'PagesController@index');
Route::get('/pages/create', 'PagesController@create');
Route::post('/pages', 'PagesController@store');
Route::get('/pages/{page}', 'PagesController@show');
Route::get('/pages/{page}/delete', 'PagesController@delete');
Route::delete('/pages/{page}', 'PagesController@destroy');
Route::get('/pages/{page}/edit', 'PagesController@edit');
Route::put('/pages/{page}', 'PagesController@update');


Route::get('/orders', 'OrdersController@index');