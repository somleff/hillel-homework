<?php

interface ActiveUser
{
    public function getFullName();
    public function getStatus();
}