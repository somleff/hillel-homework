<?php

class UserGreeter //implements ActiveUser
{
    public static function greetUser( $person )
    {
        return $person->getFullName() .' '. $person->getStatus();
    }
}