<?php

class Person implements ActiveUser
{
    public static $name;
    public static $status;


    public function __construct( $name, $status )
    {
        self::$name = $name;
        self::$status = $status;
    }


        public function getFullName()
        {
            echo static::$name;
        }


        public function getStatus()
        {
            echo static::$status;
        }
}