<?php

class Guest implements ActiveUser
{
    public static $name;
    public static $status = 'The Guest';


    public function __construct( $name )
    {
        self::$name = $name;
    }

    public function getFullName()
    {
        echo self::$name .' ';
    }


    public function getStatus()
    {
        echo self::$status;
    }
}