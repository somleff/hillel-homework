<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once ('Class/ActiveUser.class.php');
require_once ("Class/Person.class.php");
require_once ("Class/Student.class.php");
require_once ("Class/Teacher.class.php");
require_once ("Class/Admin.class.php");
require_once ('Class/Guest.class.php');
require_once ('Class/UserGreeter.class.php');

//Специально дал одинаковые имена, для наглядности
$person = new Student( 'Matt ', 'The Student' );
echo UserGreeter::greetUser($person);
echo '<hr>';

$person = new Teacher('Alex ', 'The Teacher');
echo UserGreeter::greetUser($person);
echo '<hr>';

$person = new Admin('Tim ', 'The Admin');
echo UserGreeter::greetUser($person);
echo '<hr>';

$person = new Guest('Vanya');
echo UserGreeter::greetUser($person);
echo '<hr>';
