<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once ('Class/ActiveUser.class.php');
require_once ("Class/Person.class.php");
require_once ("Class/Student.class.php");
require_once ("Class/Teacher.class.php");
require_once ("Class/Admin.class.php");
require_once ('Class/Guest.class.php');
require_once ('Class/UserGreeter.class.php');

//Специально дал одинаковые имена, для наглядности
$person = new Student();
$person->getName();
$person->getSelfStatus();
// echo UserGreeter::greetUser($person);
echo '<hr>';

$person->getFullName();
$person->getStatus();
echo '<hr>';


$person = new Teacher();
$person->getFullName();
$person->getStatus();
echo '<hr>';

$person = new Admin();
$person->getFullName();
$person->getStatus();
echo '<hr>';

$person = new Guest();
$person->getFullName();
$person->getStatus();
echo '<hr>';


















// $Student = new Student('John', 'Doe', '0679124791', 'doe@cloud.com', 'Student', '5.0', '100%');
// echo $Student->getVisitCard();
// echo '<hr>';


// $Teacher = new Teacher('Alex', 'Best', '0956785432', 'alex@cloud.com', 'Teacher', 'PHP');
// echo $Teacher->getVisitCard();
// echo '<hr>';


// $Admin = new Admin('Jane', 'Doe', '0634623478', 'jane@cloud.com', 'Administrator', 'Tuesday Thursday Saturday');

// echo Admin::getFullName();
// echo Admin::getStatus();
// //echo $Admin->getVisitCard();
// echo '<hr>';

// // $Guest = new Guest( 'Guest', 'Guest');
// // echo Guest::getFullName();
// // echo Guest::getStatus();



