<?php

$group = array(
    array('name'=>'BoJack','lastname'=>'The Horseman',
          'visits' => array(
            '21.09'=> true,
            '22.09'=> true,
            '23.09'=> false,
            '24.09'=> false,
            '25.09'=> true),
            'marks'=>array(3,4,1,1,3)
            ),
    array('name'=>'Bender','lastname'=>'The Robot',
          'visits' => array(
            '21.09'=> true,
            '22.09'=> true,
            '23.09'=> false,
            '24.09'=> false,
            '25.09'=> true),
            'marks'=>array(4,4,5,5,5)
            ),
    array('name'=>'Fynn','lastname'=>'The Human',
          'visits' => array(
            '21.09'=> true,
            '22.09'=> true,
            '23.09'=> false,
            '24.09'=> false,
            '25.09'=> true),
            'marks'=>array(5,4,4,1,5)
            ),
    array('name'=>'Jake','lastname'=>'The Dog',
          'visits' => array(
            '21.09'=> true,
            '22.09'=> true,
            '23.09'=> false,
            '24.09'=> false,
            '25.09'=> true),
            'marks'=>array(5,3,4,4,5)
            ),
    array('name'=>'Archer','lastname'=>'The Agent',
          'visits' => array(
            '21.09'=> true,
            '22.09'=> true,
            '23.09'=> false,
            '24.09'=> false,
            '25.09'=> true),
            'marks'=>array(1,1,1,1,3)
            )  
    );