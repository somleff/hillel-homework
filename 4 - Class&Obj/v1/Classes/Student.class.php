<?php

class Student
{
    public $name;
    public $lastname;
    public $average;
    public $visits;
    public $marks;
    
    
    public function __construct($name, $lastname, $visits, $marks)
    {
        $this->name = $name;
        $this->lastname = $lastname;
        $this->average = $this->getAverage($marks);
        $this->visits = $this->getVisits($visits);
        $this->marks = $marks;
    }


    public function studentName()
    {
        return $this->name. ' ' .$this->lastname;
    }



    public function getAverage($marks)
    {
        return round(array_sum($marks)/ count($marks), 1);
    }



    public function getVisits($visits)
    {
        return array_sum($visits)/count($visits)*100;
    }
}