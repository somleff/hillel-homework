<?php

class BestStudentFinder
{
    public $bestStudent;


    public function checkStudent(Student $student)
    {
        if ($student->average > 3 && $student->visits > 50)
        {
            $this->bestStudent[] = $student->studentName();
            $this->bestStudent[] = $student->average;
            //var_dump($this->bestStudent);
            return $student->studentName() .' '. $student->average;
        }
        else
        {
            return '---';
        }
    }


    public function getName()
    {
        foreach($this->bestStudent as $study)
        {
            //var_dump($this->bestStudent);
               echo "$study";
        }
    }


    public function showStudent()
    {
        $table = "";
        $table .= "<table>";
        $table .=   "<tr>";
        $table .=       "<th>Name</th>";
        $table .=       "<th>Average</th>";
        $table .=   "</tr>";
        $table .=   "<tr>";
       // $table .=       "<td>". $this->getName()  ."</td>";
        $table .=       "<td>". $this->getName() ."<hr>"."</td>";
        $table .=   "</tr>";
        $table .= "</table>";
        return $table;
    }
}