<?php

class Student
{
    public $name;
    public $lastname;
    public $marks;
    public $average;
    public $visits;


    public function __construct( $name, $lastname, $marks, $visits )
    {
        $this->name = $name;
        $this->lastname = $lastname;
        $this->marks = $marks;
        $this->average = $this->getAverage($marks);
        $this->visits = $this->getVisits($visits);
    }

    public function studentName()
    {
        return $this->name .' '. $this->lastname;
    }


    public function getAverage($marks)
    {
        return round(array_sum($marks)/ count($marks), 1);
    }


    public function getVisits($visits)
    {
        return array_sum($visits)/count($visits)*100;
    }   
}