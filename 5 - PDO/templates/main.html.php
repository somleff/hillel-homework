<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Journey Main Page</title>
  </head>
  <style>
    a { text-decoration: none; }
  </style>
  <body>
    <p><h3 align="center">Here Are All Our Tours:</h3></p>

    <table align="center" cellpadding="8" cellspacing="8" border="1">
      
      <!--Выводим список туров -->
      <?php foreach ($tours as $tour): ?>
            <tr>
              <td align="center">
              <b><a href="tour_info.php?info=<?=$tour['id']?>">
              <?php echo htmlspecialchars($tour['name'], ENT_QUOTES, 'UTF-8'); ?></a></b><br>

              <a href="tour_info.php?info=<?=$tour['id']?>">
              <img src="<?php echo htmlspecialchars($tour['img'], ENT_QUOTES, 'UTF-8'); ?>" width="240" height="180"></a><br>

              <b><?php echo htmlspecialchars($tour['price'], ENT_QUOTES, 'UTF-8'); ?> $</b>
              </td>   
            </tr>
            <?php endforeach; ?>
    </table>
  </body>
</html>