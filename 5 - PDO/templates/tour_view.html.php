<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tour Information</title>
</head>
<body>
    <table cellpadding="8" cellspacing="8" border="1">
        <tr>
            <th>Name</th>
            <th>About</th>
            <th>Price</th>
            <th>Preview</th>
            <th>add</th>
        </tr>

        <?php foreach ($tours as $tour): ?>
        <tr>
            <td><b><?php echo htmlspecialchars($tour['name'], ENT_QUOTES, 'UTF-8'); ?></b></td>

            <td><?php echo htmlspecialchars($tour['desc'], ENT_QUOTES, 'UTF-8'); ?></td> 

            <td><b><?php echo htmlspecialchars($tour['price'], ENT_QUOTES, 'UTF-8'); ?></b></td>

            <td><img src="<?php echo htmlspecialchars($tour['img'], ENT_QUOTES, 'UTF-8'); ?>" width="240" height="180"></td>

            <td><a href="buy_tour.php?buy=<?=$tour['id']?>"><input type="submit" value="Buy"></a></td>
        </tr>
    <?php endforeach; ?>
    </table>
    <br>
    <?php echo '<a href="index.php"><input type="submit" value="Main Page"></a>'; ?>
</body>
</html>