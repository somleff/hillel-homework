<?php

include 'db_&_slq/db.inc.php';

# Из DB извлекаем подробности тура
if(isset($_GET['info']))
{
    try
    {
        $sql = 'SELECT id, tour_name, tour_desc, price, img FROM tour_list
                WHERE id = :id';
        $s = $pdo->prepare($sql);
        $s->bindValue(':id', $_GET['info']);
        $s->execute();
    }
    catch(PDOEXception $e)
    {
        echo 'Error fetching tour_info: ' . $e->getMessage();
    }

    foreach ($s as $row)
    {
        $tours[] = array(
            'id' => $row['id'],
            'name' => $row['tour_name'],
            'desc' => $row['tour_desc'],
            'price' => $row['price'],
            'img' => $row['img']
            );
    }
    include 'templates/tour_view.html.php';
}

else
{
    echo 'Akela Missed!';
}