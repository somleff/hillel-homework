<?php

#Подключение к DB.
include 'db_&_slq/db.inc.php';

try
{
    $sql = 'SELECT id, tour_name, tour_desc, price, img FROM tour_list';
    $result = $pdo->query($sql);
}
catch (PDOException $e)
{
    echo 'Error fetching tour_list: ' . $e->getMessage();
}

foreach ($result as $row)
{
  $tours[] = array(
      'id' => $row['id'],
      'name' => $row['tour_name'],
      'desc' => $row['tour_desc'],
      'price' => $row['price'],
      'img' => $row['img']
    );
}

include 'templates/main.html.php';