<?php

#Создаем таблицу tour_list
try
{
    $sql = 'CREATE TABLE tour_list(
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            tour_name TEXT,
            price INT NOT NULL,
            tour_desc TEXT,
            img TEXT
            )DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
    $pdo->exec($sql);
}
catch (PDOException $e)
{
    echo 'Error creating tour_list table: ' . $e->getMessage();
}

#_________________________________
#Создаем таблицу order_list
try
{
    $sql = 'CREATE TABLE order_list(
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            client_name TEXT,
            phone INT NOT NULL,
            email TEXT,
            id_tour INT,
            amount INT
            )DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
    $pdo->exec($sql);
}
catch (PDOException $e)
{
    echo 'Error creating order_list table: ' . $e->getMessage();
}
