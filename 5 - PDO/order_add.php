<?php

#Добавляем заказ в DB таблица клиента
include 'db_&_slq/db.inc.php';

if(isset($_POST))
{
    try
    {
        $sql = 'INSERT INTO order_list SET
                client_name = :client_name,
                phone = :phone,
                email = :email,
                id_tour = :id_tour,
                amount = :amount';
        $s = $pdo->prepare($sql);
        $s->bindValue(':client_name', $_POST['name']);
        $s->bindValue(':phone', $_POST['phone']);
        $s->bindValue(':email', $_POST['email']);
        $s->bindValue(':id_tour', $_POST['id_tour']);
        $s->bindValue(':amount', $_POST['person']);
        $s->execute();

        header('Location: index.php');
        exit();
    }
    catch (PDOException $e)
    {
        echo 'Error adding order: ' . $e->getMessage();
    }
}
/*['name']) && isset($_POST['phone'])  && isset($_POST['email'] && isset($_POST['person'])*/