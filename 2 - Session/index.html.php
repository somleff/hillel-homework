<?php

session_start();

$shop = array(
    array('title' => 'Маскарпоне', 'price' => '1500'),
    array('title' => 'Реблошон', 'price' => '2000'),
    array('title' => 'Эмменталь', 'price' => '1800'),
    array('title' => 'Камамбер', 'price' => '1000'),
    array('title' => 'Горгонзола', 'price' => '3100'),
    array('title' => 'Шардоне', 'price' => '540'),
    array('title' => 'Мускат', 'price' => '500'),
    array('title' => 'Мерло', 'price' => '690'),
    array('title' => 'Розе', 'price' => '350'),
    array('title' => 'Брют', 'price' => '990'),
    );
?>

    <table cellpadding="8" cellspacing="0" border="1">
    <tr>
        <th>Id</th>
        <th>Product</th>
        <th>Price</th>
        <th></th>
    </tr>
        <?php foreach($shop as $sKey=>$item): ?>
            <tr>
                <td><?= $sKey?></td>
                <td><?= $item['title'] ?></td>
                <td><?= $item['price'] ?></td>
                <td><button name="buy_item">
                <a href="cart_add.php?iid=<?= $sKey ?>">Add</a></button></td>
            </tr>
        <?php endforeach; ?>
    </table>

<?php if($_SESSION['cart']):?>
    <hr>
    <h2>Ваша корзина:</h2>
    <?php foreach($_SESSION['cart'] as $ckey => $ammount):?>
        <p>
            <b><?=$shop[$ckey]['title']?></b>
            x <?=$ammount?>
        </p>
    <?php endforeach;?>
<?php endif;?>