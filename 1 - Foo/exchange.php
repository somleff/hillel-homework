<?php

function currency($uah)
{
    $euro = round($uah/30.3, 2);
    $usd = round($uah/26.0, 2);
    $result = 'EUR: '. $euro ."<br/>";
    $result .= 'USD: '. $usd ."<hr/>";
    return ($result);
}

$uah=1000;
while ($uah<=2000) {
    echo 'UAH: '. $uah . "<br/>";
    echo currency($uah);
    $uah+=50;
}

/*----------------Старый Вариант-----------------

<?php

function currency($uah)
{
    $euro = round($uah/30.3, 2);
    $usd = round($uah/26.0, 2);
    echo 'UAH: '. $uah . "<br/>";
    echo 'EUR: '. $euro ."<br/>";
    echo 'USD: '. $usd ."<hr/>";

}

$uah=1000;
while ($uah<=2000) {
    currency($uah);
    $uah+=50;
}

/*-----------Вариант №2-------------

<?php

function currency($uah)
{
    $euro = round($uah/30.3, 2);
    $usd = round($uah/26.0, 2);
    echo 'EUR: '. $euro ."<br/>";
    echo 'USD: '. $usd ."<hr/>";
}

$uah=1000;
while ($uah<=2000) {
    echo 'UAH: '. $uah . "<br/>";
    currency($uah);
    $uah+=50;
}

/*------------Вариант с global--------------------

<?php

function currency($uah)
{
    global $euro;
    global $usd;
    $euro = round($uah/30.3, 2);
    $usd = round($uah/26.0, 2);
    
}

$uah=1000;
while ($uah<=2000) {
    echo 'UAH: '. $uah . "<br/>";
    currency($uah);
    echo 'EUR: '. $euro ."<br/>";
    echo 'USD: '. $usd ."<hr/>";
    $uah+=50;
}

/* --------Вариант global округления №2/ Не в теле функции/----------------

<?php

function currency($uah)
{
    global $euro;
    global $usd;
    $euro = $uah/30.3;
    $usd = $uah/26.0;
}

$uah=1000;
while ($uah<=2000) {
    echo 'UAH: '. $uah . "<br/>";
    currency($uah);
    echo 'EUR: '. round($euro, 2) ."<br/>";
    echo 'USD: ' . round($usd, 2) ."<hr/>";
    $uah+=50;
}

Можно еще вариантов придумать, но зачем?;)
*/