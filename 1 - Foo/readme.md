## Домашнее задание №1


- Создаем страничку, где при помощи цикла while выводим таблицу конвертации гривны в доллары и евро от 1000 грн до 2000 с шагом в 50(20 итераций). Полученные значения округяем до сотых. Для конвертации создаем специальную функцию(которая переводит в нужный курс и округляет до сотых).

- Создаем массив из 10 товаров, каждый из которых имеет структуру:

{'title' => 'название товара', 'price' =>20, 'category' => 'категория' }

Всем товарам присваеваем одну из двух категорий - food и drink.

В итоге должен получится двухмерный массив. (Внутри нуммерованого массива, 10 элементов которые являются ассоциативными массивами)

При помощи цикла foreach отображаем товары в виде таблицы содержащей 3 столбика. Заголовки столбиков соответственно: Название товара, категория и цена.

- Дополнительное задание: Для товаров из категории drink - делаем скидку 10% при помощи арифметических операторов.
