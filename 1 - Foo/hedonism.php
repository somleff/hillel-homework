<?php

$hedonism = array(
    array('title' => 'Наименование', 'price' => 'Цена', 'category' => 'Категория'),
	array('title' => 'Маскарпоне', 'price' => '1500','category' => 'cheese'),
    array('title' => 'Реблошон', 'price' => '2000','category' => 'cheese'),
    array('title' => 'Эмменталь', 'price' => '1800','category' => 'cheese'),
    array('title' => 'Камамбер', 'price' => '1000','category' => 'cheese'),
    array('title' => 'Горгонзола', 'price' => '3100','category' => 'cheese'),
    array('title' => 'Шардоне', 'price' => '540','category' => 'wine (-10%)'),
    array('title' => 'Мускат', 'price' => '500','category' => 'wine (-10%)'),
    array('title' => 'Мерло', 'price' => '690','category' => 'wine (-10%)'),
    array('title' => 'Розе', 'price' => '350','category' => 'wine (-10%)'),
    array('title' => 'Брют', 'price' => '990','category' => 'wine (-10%)'),
);

echo '<table cellpadding="5" cellspacing="0" border="1">';
foreach ($hedonism as $key => $value) {
    echo "<tr>";

    if ($value['category']=='wine (-10%)') {
        $value['price']*=0.9;
    }

    foreach ($value as $data)
        echo "<td>" .$data. "</td>";
    echo "</tr>";
}
echo "</table>";


