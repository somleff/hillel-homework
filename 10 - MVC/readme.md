## Домашнее задание 10


 - Используем файлы с лекции.

- Добавляем базу данных в проект.

 - В родительский класс модели (core/Model) добавляем в конструкторе подключение к базе данных и сохраняем его в свойство db.

 - Создаем в базе данных таблицу portfolio. В ней 4 колонки - id, year, url, description. Получаем данные в модели, обрабатываем и отображаем в экшене index контролера portfolio.

 - Добавляем таблицу articles. У нее колонки - id, title, text. Для нее создаем отдельный контроллер, модель и отображение. Выводим все записи в методе index. 

 - Ознакамливаемся с материалом лекции и статьей.

 - Добавляем функционал, чтобы (любой)контроллер вызывался независимо от регистра букв. (mAin, main, MAIN во всех случаях должен быть вызван контроллер Main)

 - Добавляем проверку наличия 3 сегмента в ссылке. Например example.com/products/show/3(3 находится в третьем сегменте). Если 3 сегмент присутствует - передаем его в качества аргумента в вызываемый метод контроллера.

 - На основании этого функционала делаем ссылки на просморт работ портфолио и записей(articles) вида название_сайта.dev/название контролера/метод/id записи
