<div>
        <?php foreach ($data as $result): ?>

            <h1>
                <?php echo htmlspecialchars($result['title'], ENT_QUOTES, 'UTF-8'); ?>
            </h1>
            <hr>
            <p>
                <?php echo htmlspecialchars($result['text'], ENT_QUOTES, 'UTF-8'); ?>
            </p>
        <?php endforeach; ?>
           
</div>