<div>
    <h1>
        Personal Portfolio Page
    </h1>
    <ul>
         <?php foreach ($data as $result): ?>
         <li>
             <b>Year: </b><?php echo htmlspecialchars($result['year'], ENT_QUOTES, 'UTF-8'); ?>
         </li>  
         <li>
             <b>Url: </b><a href="<?php echo htmlspecialchars($result['url'], ENT_QUOTES, 'UTF-8'); ?>">IT School</a>
         </li> 
         <li>
             <b>Desc: </b><?php echo htmlspecialchars($result['description'], ENT_QUOTES, 'UTF-8'); ?>
         </li> 
         <hr>   
        <?php endforeach; ?>
    </ul>
</div>