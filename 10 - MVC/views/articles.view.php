<div>
    <h1>
        Articles Page
    </h1>
        <?php foreach ($data as $result): ?>

            <h3>
                <?php echo htmlspecialchars($result['title'], ENT_QUOTES, 'UTF-8'); ?>
            </h3>
            <hr>
            <p>
                <?php echo htmlspecialchars($result['text'], ENT_QUOTES, 'UTF-8'); ?>
            </p>
             <p>
                <a href="/Articles/show/?id=<?=$result['id'];?>">More Info</a>
            </p>
        <?php endforeach; ?>
           
</div>