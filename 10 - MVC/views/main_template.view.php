<!DOCTYPE html>
<html>
<head>
	<title>My Home page</title>
  <style>
   a { 
    text-decoration: none;
   } 
  </style>
</head>
<body>

<div class='header'>
	<ul>
			<a href="/">Home page</a>
			<a href="/Articles/index">Articles</a>
			<a href="/Portfolio/index">Portfolio</a>
			<a href="/Contacts/index">Contacts</a>	
	</ul>
</div>

<?php include'views/'.$content_view; ?>

<div class="footer">
	<p>
		Hillel student Homepage (c)2017
	</p>
</div>

</body>
</html>