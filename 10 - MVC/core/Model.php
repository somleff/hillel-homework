<?php
class Model
{
    public $db;


    public function __construct()
    {
        try
        {
            $pdo = new PDO('mysql:host=localhost;dbname=mvc_db', 'root', '21092015');
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->exec('SET NAMES "utf8"');

            $this->db = $pdo;
        }
        catch (PDOException $e)
        {
            echo 'Unable to connect to the database server:' . $e->getMessage();
        }	
}
}