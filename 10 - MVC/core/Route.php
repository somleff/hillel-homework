<?php
class Route
{
	static function start()
	{
		$controllerName = 'Main';
		$actionName = 'index';
		$routes = explode('/', $_SERVER['REQUEST_URI']);


		if(!empty($routes[1]))
		{
			$controllerName = $routes[1];
		}

			if(!empty($routes[2]))
			{
				$actionName = $routes[2];
			}


		//добавляем префиксы

		$modelName = 'Model_'.$controllerName;
		$controllerName = 'Controller_'.$controllerName;
		//$actionName = 'action_'.$actionName;
		$modelFile = ucfirst($modelName). '.php';


		if(isset($routes[3]))
		{
			$actionName = 'action_show';
		}
		else
		{
			$actionName = 'action_'.$actionName;
		}

		if(file_exists('models/'.$modelFile))
		{
			include 'models/'.$modelFile;
		}


		$controllerFile = $controllerName.'.php';


		if(file_exists('controllers/'.$controllerFile))
		{
			include 'controllers/'.$controllerFile;
		}
		else
		{
			Route::ErrorPage404();
		}


		//new Controller_Book;
		$controller = new $controllerName;
		//action_index 
		$action = $actionName;


		if(method_exists($controller, $action))
		{
			//$controller->action_index();
			$controller -> $action();
		}
		else
		{
			Route::ErrorPage404();
		}
	}
	public static function ErrorPage404()
	{
		echo "Page not found!!!";
		die();
	}
}