<?php
class Controller_Articles extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new Model_Articles();
    }


    public function action_index()
    {
        $data = $this->model->getArticles();
        $this->view->generate('articles.view.php', $data);
    }


    public function action_show()
    {
        $data = $this->model->showArticle();
        $this->view->generate('showarticles.view.php', $data);
    }
}