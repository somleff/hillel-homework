<?php
class Controller_Portfolio extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new Model_Portfolio();
    }


    public function action_index()
    {
        $data = $this->model->getPortfolio();
        $this->view->generate('portfolio.view.php', $data);
    }


    public function action_show()
    {
        $data = $this->model->showPortfolio();
        $this->view->generate('showportfolio.view.php', $data);
    }
}
