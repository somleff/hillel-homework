<?php
class Controller_Contacts extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new Model_Contacts();
    } 


    public function action_index()
    {
        $data = $this->model->getContacts();
        $this->view->generate('contacts.view.php', $data);
    }
}