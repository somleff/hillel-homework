<?php
class Controller_Main extends Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_Main();
	}


	public function action_index()
	{
		$this->view->generate('home_page.view.php');
	}
}