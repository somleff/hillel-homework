<?php

class Model_Portfolio extends Model
{
    public function getPortfolio()
    {
        try
        {
            $sql = 'SELECT * FROM portfolio';
            $stmt = $this->db->query($sql);
            $result = $stmt->fetchAll();
            return $result;

        }
        catch (PDOException $e)
        {
            echo 'Error fetching tour_list: ' . $e->getMessage();
        }
    }


    public function showPortfolio()
    {
        $id = $_GET['id'];
        try
        {
            $sql = 'SELECT * FROM portfolio WHERE id=' .$id;
            $stmt = $this->db->query($sql);
            $result = $stmt->fetchAll();
            return $result;

        }
        catch (PDOException $e)
        {
            echo 'Error fetching tour_list: ' . $e->getMessage();
        }
    }  
}