<?php

class Model_Articles extends Model
{
    public function getArticles()
    {
        try
        {
            $sql = 'SELECT * FROM articles';
            $stmt = $this->db->query($sql);
            $result = $stmt->fetchAll();
            return $result;

        }
        catch (PDOException $e)
        {
            echo 'Error fetching tour_list: ' . $e->getMessage();
        }
    }


    public function showArticle()
    {
        $id = $_GET['id'];
        try
        {
            $sql = 'SELECT * FROM articles WHERE id='.$id;
            $stmt = $this->db->query($sql);
            $result = $stmt->fetchAll();
            return $result;

        }
        catch (PDOException $e)
        {
            echo 'Error fetching tour_list: ' . $e->getMessage();
        }
    }
}