<?php

include "Class/BaseClass.class.php";
include "Class/Student.class.php";
include "Class/Teacher.class.php";
include "Class/Admin.class.php";


$Student = new Student('John', 'Doe', '0679124791', 'doe@cloud.com', 'Student', '5.0', '100%');

echo $Student->getVisitCard();
echo '<hr>';


$Teacher = new Teacher('Alex', 'Best', '0956785432', 'alex@cloud.com', 'Teacher', 'PHP');

echo $Teacher->getVisitCard();
echo '<hr>';


$Admin = new Admin('Jane', 'Doe', '0634623478', 'jane@cloud.com', 'Administrator', 'Tuesday Thursday Saturday');

echo $Admin->getVisitCard();