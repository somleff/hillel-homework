<?php

abstract class BaseClass 
{
    public $name = "";
    public $lastname = "";
    public $phone = "";
    public $email = "";
    public $role = "";

    public function __construct( $name, $lastname, $phone, $email, $role )
    {
        $this->name = $name;
        $this->lastname = $lastname;
        $this->phone = $phone;
        $this->email = $email;
        $this->role = $role;
    }

    abstract public function getVisitCard();
}