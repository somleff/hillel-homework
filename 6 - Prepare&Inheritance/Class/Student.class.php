<?php

class Student extends BaseClass
{
    public $average;
    public $attendance;

    public function __construct($name, $lastname, $phone, $email, $role, $average, $attendance)
    {
        parent:: __construct($name, $lastname, $phone, $email, $role);
            $this->average = $average;
            $this->attendance = $attendance;
    }

    public function getVisitCard()
    {
      return $this->name .' '. 
             $this->lastname .', '.
             $this->phone .', '.
             $this->email .', '.
             $this->role .', '.
            'Average: ' .$this->average .', '.
            'Attendance: ' .$this->attendance;
    }
}