<?php

class Teacher extends BaseClass
{
    public $subject;

    public function __construct($name, $lastname, $phone, $email, $role, $subject)
    {
        parent:: __construct($name, $lastname, $phone, $email, $role);
        $this->subject = $subject;
    }

    public function getVisitCard()
    {
      return $this->name .' '. 
             $this->lastname .', '.
             $this->phone .', '.
             $this->email .', '.
             $this->role .', '.
             $this->subject;
    }
}