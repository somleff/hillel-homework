<?php

class Admin extends BaseClass
{
    public $schedule;

    public function __construct($name, $lastname, $phone, $email, $role, $schedule)
    {
        parent:: __construct($name, $lastname, $phone, $email, $role);
        $this->schedule = $schedule;
    }

    public function getVisitCard()
    {
      return $this->name .' '. 
             $this->lastname .', '.
             $this->phone .', '.
             $this->email .', '.
             $this->role .', '.
             'Schedule: ' .$this->schedule;
    }
}