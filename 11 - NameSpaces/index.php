<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use App\WebObject\Cart\Cart as C;
use App\WebObject\Cart\ExChange as E;

function __autoload($class){
    $path = 'Classes/'.str_replace('\\', '/', $class);
    $path = $path.'.class.php';
    require_once($path);
}


$shop = new C;
$item = array('name'=>'MacBook', 'price'=>30000);
$item1 = array('name'=>'iPod', 'price'=>3200);

echo C::addProduct($item);
echo C::addProduct($item1);
echo C::saveCart();
echo C::getProducts();

$change = new E;
echo '<hr>';
echo E::currency();