<?php

namespace App\WebObject\Cart;

class Cart
{
    public static $products = array();


    public function __construct()
    {
        if(isset($_COOKIE['cart']))
        {
            $products = json_decode($_COOKIE['cart'], true);
            self::$products = $products;
        }
        // else
        // {
        //     echo 'Basket is Empty!';
        // }
    }


    public static function addProduct($item)
    {
        array_push(self::$products, $item);
    }


    public static function saveCart()
    {
         $cart_json = json_encode(self::$products);
         setcookie('cart', $cart_json, time()+60*60);
    }


    public static function getProducts()
    {
        foreach(self::$products as $product)
        {
            foreach($product as $item)
            {
             echo "$item".'<br>';   
            }
            
        }
    }
}