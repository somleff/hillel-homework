        <table cellpadding="8" cellspacing="0" border="1">
    <tr>
        <th>Id</th>
        <th>Product</th>
        <th>Price</th>
        <th>About</th>
        <th></th>
    </tr>
        <?php foreach($shop as $sKey=>$sValue): ?>
            <tr>
                <td><?= $sKey?></td>
                <td><?= $sValue['title'] ?></td>
                <td><?= $sValue['price'] ?></td>
                <td><button name="desc">
                <a href="view.php?id=<?= $sKey ?>">See Me</a></button></td>
                <td><button name="buy">
                <a href="buy.php?id=<?= $sKey ?>">Buy Me</a></button></td>
            </tr>
        <?php endforeach; ?>
    </table>

<h2>Basket: </h2>
<?php if($cart): ?>
    <?php foreach($cart as $cKey => $sum): ?>
        <p>
            <b><?= $shop[$cKey]['title']?></b>
            x <?= $sum ?>

                <button name="delete">
                <a href="delete.php?delete=<?= $cKey ?>">Delete Me</a></button>

                <button name="deleteAll">
                <a href="deleteall.php?del=<?= $cKey ?>">Delete All</a></button>
        </p>
    <?php endforeach; ?>
<?php endif; ?>