<?php

if(isset($_GET['delete'])){
    $delete = $_GET['delete'];

    if(isset($_COOKIE['cart'])){
        $cart = json_decode($_COOKIE['cart'], true);

        if($cart[$delete] == 1){
            unset($cart[$delete]);
        }else{
            $cart[$delete] --;
        }
    }

    $cart_json = json_encode($cart);
    setcookie('cart', $cart_json, time()+3600);

    header('Location: index.php');    
}
else{
    echo "Wrong";
}



