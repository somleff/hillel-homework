<?php

if(isset($_GET['id']))
{
    $sKey = $_GET['id'];
    $cart = array();

    if(isset($_COOKIE['cart']))
    {
        $cart = json_decode($_COOKIE['cart'], true);
    }

    if(isset($cart[$sKey]))
    {
        $cart[$sKey]++;
    }
    else{
        $cart[$sKey] = 1;
    }
    $cart_json = json_encode($cart);
    setcookie('cart', $cart_json, time()+60*60);
    header('Location: index.php?item_add = 1');
}
else{
    echo 'Wrong!!!';
}