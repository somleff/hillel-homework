<?php

if(isset($_GET['del'])){
    $cKey = $_GET['del'];

    if(isset($_COOKIE['cart']))
    {
        $cart = json_decode($_COOKIE['cart'], true);
    }
    $cart_json = json_encode($cart[$cKey]);
    setcookie('cart', $cart_json, time()-3600);

    header('Location: index.php?item_del = 0');
}